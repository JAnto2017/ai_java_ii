package AI_1.ai_java_ii;

public class HechoBooleano implements IHecho{
    
    //String Nombre();
    //Object Valor();
    //int Nivel();
    //String Pregunta();
    //void setNivel(int l);

    protected String nombre;
    @Override
    public String Nombre(){
        return nombre;
    }

    protected boolean valor;
    @Override
    public Object Valor(){
        return valor;
    }

    protected int nivel;
    @Override
    public int Nivel(){
        return nivel;
    }

    @Override
    public void setNivel(int i) {
        nivel = i;
    }

    protected String pregunta = null;
    @Override
    public String Pregunta() {
        return pregunta;
    }

    //constructor
    public HechoBooleano(String _nombre,boolean _valor,String _pregunta,int _nivel) {
        nombre = _nombre;
        valor = _valor;
        pregunta = _pregunta;
        nivel = _nivel;
    }

    @Override
    public String toString() {
        String res = "";
        if (!valor) {
            res += "!";
        }
        res += nombre + " (" + nivel + ")";
        return res;
    }

}
