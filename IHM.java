package AI_1.ai_java_ii;

import java.util.ArrayList;

import AI_1.ai_java_ii.Regla;

public interface IHM {
    int PedirValorEntero(String pregunta);
    boolean PedirValorBoolean(String pregunta);
    void MostrarHechos(ArrayList<IHecho> hechos);
    void MostrarReglas(ArrayList<Regla> reglas);
}
