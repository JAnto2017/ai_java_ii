package AI_1.ai_java_ii;

public interface IHecho {
    String Nombre();
    Object Valor();
    int Nivel();
    String Pregunta();
    void setNivel(int l);
}
