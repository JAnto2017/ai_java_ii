package AI_1.ai_java_ii;

import java.util.ArrayList;

import AI_1.ai_java_ii.Regla;

public class MotorInferencia {
    private BaseDeHechos bdh;
    private BaseDeReglas bdr;
    private IHM ihm;
    private int nivelMaxRegla;
    //-----------------------------------------------------------------------------------------
    public MotorInferencia(IHM _ihm) {
        ihm = _ihm;
        bdh = new BaseDeHechos();
        bdr = new BaseDeReglas();
    }
    //-----------------------------------------------------------------------------------------
    int PedirValorEntero(String pregunta) {
        return ihm.PedirValorEntero(pregunta);
    }
    //-----------------------------------------------------------------------------------------
    boolean PedirValorBoolean(String pregunta) {
        return ihm.PedirValorBoolean(pregunta);
    }
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    int EsAplicable(Regla _r) {
        int nivelMax = -1;
        for (IHecho h : _r.getPremisas()) {
            IHecho hechoEncontrado = bdh.Buscar(h.Nombre());
            if (hechoEncontrado == null) {
                if (h.Pregunta() != null) {
                    hechoEncontrado = HechoFactory.Hecho(h, this);
                    bdh.AgregarHecho(hechoEncontrado);
                } else {
                    return -1;
                }
            }

            if (!hechoEncontrado.Valor().equals(h.Valor())) {                
                //nivelMax = Math.max(nivelMax, hechoEncontrado.Nivel());
                return -1;
            } else {
                nivelMax = Math.max(nivelMax, hechoEncontrado.Nivel());
            }
        }
        return nivelMax;
    }
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    Regla EncontrarRegla(BaseDeReglas bdrLocal) {
        for (Regla r : bdrLocal.getReglas()) {
            int nivel = EsAplicable(r);
            if (nivel != -1) {
                nivelMaxRegla = nivel;
                return r;
            }
        }
        return null;
    }
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    public void Resolver() {
        BaseDeReglas bdrLocal = new BaseDeReglas();
        bdrLocal.setReglas(bdr.getReglas());

        bdh.vaciar();

        Regla r = EncontrarRegla(bdrLocal);
        while (r != null) {
            IHecho nuevoHecho = r.conclusion;
            nuevoHecho.setNivel(nivelMaxRegla + 1);
            bdh.AgregarHecho(nuevoHecho);
            bdrLocal.eliminar(r);
            r = EncontrarRegla(bdrLocal);            
        }
        ihm.MostrarHechos(bdh.gHechos());
    }
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    public void agregarRegla(String str) {
        //separacion nombre : regla
        String[] nombreRegla = str.split(":");
        if (nombreRegla.length ==  2) {
            String nombre = nombreRegla[0].trim();
            //separacion premisas THEN conclusion
            String regla = nombreRegla[1].trim();
            regla = regla.replaceFirst("^" + "IF", "");
            String[] premisasConclusion = regla.split("THEN");

            if (premisasConclusion.length == 2) {
                //lectura de las premisas
                ArrayList<IHecho> premisas = new ArrayList<>();
                String[] premisasStr = premisasConclusion[0].split(" AND ");

                for (String cadena : premisasStr) {
                    IHecho premisa = HechoFactory.Hecho(cadena.trim());
                    premisas.add(premisa);
                }

                //lectura de la conclusion
                String conclusionStr = premisasConclusion[1].trim();
                IHecho conclusion = HechoFactory.Hecho(conclusionStr);

                //creacion de la regla e incorporacion a la base
                bdr.agregarRegla(new Regla(nombre, premisas, conclusion));
            }
        }
    }
}
