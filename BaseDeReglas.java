package AI_1.ai_java_ii;

import java.util.ArrayList;

public class BaseDeReglas {
    protected ArrayList<Regla> reglas;

    public void setReglas(ArrayList<Regla> _reglas) {
        for (Regla r : _reglas) {
            Regla copia = new Regla(r.nombre, r.premisas, r.conclusion);
            reglas.add(copia);
        }
    }
    public ArrayList<Regla> getReglas() {
        return reglas;
    }
    //----------------------------------------------------------------------------------------- CONSTRUCTOR
    public BaseDeReglas() {
        reglas = new ArrayList<>();
    }
    //-----------------------------------------------------------------------------------------
    public void clearBase() {
        reglas.clear();
    }
    //-----------------------------------------------------------------------------------------
    public void agregarRegla(Regla e) {
        reglas.add(e);
    }
    //-----------------------------------------------------------------------------------------
    public void eliminar(Regla r) {
        reglas.remove(r);
    }
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
}
