package AI_1.ai_java_ii;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;

public class HechoFactory {
    static IHecho Hecho(IHecho h, MotorInferencia m) {
        try {
            IHecho nuevoHecho;
            Class clase = h.getClass();
            if (clase.equals(Class.forName("AI_1.ai_java_ii.HechoEntero"))) {
                nuevoHecho = CrearHechoEntero (h,m);
            } else {
                nuevoHecho = CrearHechoBooleano(h,m);
            }
            return nuevoHecho;
        } catch (ClassNotFoundException ex) {
            //e.printStackTrace();
            //Logger.getLogger(HechoFactory.class.getName()).log(Level.SEVERE,null,ex);
            System.err.println("Error excepción no clase HechoEntero encontrada ---------------- ");
            ex.printStackTrace();
            return null;
        }
    }
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    static IHecho CrearHechoEntero(IHecho f, MotorInferencia m) {
        int valor = m.PedirValorEntero(f.Pregunta());
        return new HechoEntero(f.Nombre(), valor, null, 0);
    }
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    static IHecho CrearHechoBooleano(IHecho f, MotorInferencia m) {
        boolean valorB = m.PedirValorBoolean(f.Pregunta());
        return new HechoBooleano(f.Nombre(), valorB, null, 0);
    }
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    static IHecho Hecho(String hechoStr) {
        hechoStr = hechoStr.trim();
        if (hechoStr.contains("=")) {

            //existe "=" es un hecho entero
            hechoStr = hechoStr.replaceFirst("^" + "\\(","");
            String[] nombreValorPregunta = hechoStr.split("[=()]");

            if (nombreValorPregunta.length >= 2) {
                String pregunta = null;
                if (nombreValorPregunta.length == 3) {
                    pregunta = nombreValorPregunta[2].trim();
                }
                return new HechoEntero(nombreValorPregunta[0].trim(), Integer.parseInt(nombreValorPregunta[1].trim()), pregunta, 0);
            }
        } else {

            //hecho booleano nombre[pregunta] o !nombre[pregunta]
            boolean valor = true;
            if (hechoStr.startsWith("!")) {
                valor = false;
                hechoStr = hechoStr.substring(1).trim();
            }
            // Split tras eliminar el primer delimitador si es necesario : "("
            hechoStr = hechoStr.replaceFirst("^" + "\\(", "");
            String[] nombrePregunta = hechoStr.split("[()]");
            String pregunta = null;

            if (nombrePregunta.length == 2) {
                pregunta = nombrePregunta[1].trim();
            }
            return new HechoBooleano(nombrePregunta[0].trim(), valor, pregunta, 0);
        }
        return null;
    }
}
