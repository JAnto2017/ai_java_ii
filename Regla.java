package AI_1.ai_java_ii;

import java.util.ArrayList;
import java.util.StringJoiner;

import AI_1.ai_java_ii.IHecho;

public class Regla {
    //-----------------------------------------------------------------------------------------------------------------------
    protected ArrayList<IHecho> premisas;
    public ArrayList<IHecho> getPremisas() {
        return premisas;
    }
    public void setPremisas(ArrayList<IHecho> premisas) {
        this.premisas = premisas;
    }
    //-----------------------------------------------------------------------------------------------------------------------
    protected IHecho conclusion;
    public IHecho getConclusion() {
        return conclusion;
    }
    public void setConclusion(IHecho conclusion) {
        this.conclusion = conclusion;
    }
    //-----------------------------------------------------------------------------------------------------------------------
    protected String nombre;
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    //----------------------------------------------------------------------------------------------------------------------- CONSTRUCTOR
    public Regla(String nombre,ArrayList<IHecho> premisas,IHecho conclusion) {
        this.nombre = nombre;
        this.premisas = premisas;
        this.conclusion = conclusion;
    }

    //-----------------------------------------------------------------------------------------------------------------------
    @Override
    public String toString() {
        String cadena = nombre + " : IF (";

        StringJoiner sj = new StringJoiner(" AND ");
        for (IHecho hecho : premisas) {
            sj.add(hecho.toString());
        }

        cadena += sj.toString() + ") THEN " + conclusion.toString();    
        return cadena;
    }
}
