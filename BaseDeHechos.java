package AI_1.ai_java_ii;

import java.util.ArrayList;

public class BaseDeHechos {
    protected ArrayList<IHecho> hechos;

    public ArrayList<IHecho> gHechos() {
        return hechos;
    }

    //constructor
    public BaseDeHechos() {
        hechos = new ArrayList<IHecho>();
    }

    //vaciar los IHechos
    public void vaciar() {
        hechos.clear();
    }

    //añadir hecho
    public void AgregarHecho(IHecho hecho){
        hechos.add(hecho);
    }

    //buscar un hecho en la base IHecho
    public IHecho Buscar(String nombre){
        for(IHecho hecho : hechos){
            if (hecho.Nombre().equals(nombre)) {
                return hecho;
            }
        }
        return null;
    }

    //encontrar el valor de un hecho en la base a partir de su nombre pasado como parámetro
    public Object RecuperarValorHecho(String nombre) {
        for (IHecho hecho : hechos) {
            if (hecho.Nombre().equals(nombre)) {
                return hecho.Valor();
            }
        }
        return null;
    }
}
