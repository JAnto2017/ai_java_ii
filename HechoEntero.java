package AI_1.ai_java_ii;

public class HechoEntero implements IHecho{
    
    protected String nombre;
    @Override
    public String Nombre(){
        return nombre;
    }

    protected int valor;
    @Override
    public Object Valor(){
        return valor;
    }

    protected int nivel;
    @Override
    public int Nivel(){
        return nivel;
    }

    @Override
    public void setNivel(int i) {
        nivel = i;
    }

    protected String pregunta = "";
    @Override
    public String Pregunta() {
        return pregunta;
    }

    //String Nombre();
    //Object Valor();
    //int Nivel();
    //String Pregunta();
    //void setNivel(int l);

    //constructor
    public HechoEntero(String _nombre,int _valor,String _pregunta,int _nivel) {
        nombre = _nombre;
        valor = _valor;
        pregunta = _pregunta;
        nivel = _nivel;
    }

    @Override
    public String toString() {
        return nombre + "=" + valor +" (" + nivel + ")";
    }
}
