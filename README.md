# INTELIGENCIA ARTIFICIAL USANDO JAVA #

Sistema codificado en Java, con salida por consola.

## REQUISITOS ##

Dispondrán distintos tipos de hechos:
1. Hechos cuyo valor es un número entero, como puede ser el orden o número de lados.
2. Hechos cuyo valor es booleano, como la presncia o no de ángulos rectos o triángulo.

## IMPLEMENTACIÓN ##

Un hecho puede ser:
1. Un nombre.
2. Un valor.
3. Un nivel, correspondiente al lugar dentro del árbol de decisiones.
4. Una pregunta, planteada al usuario.

### REGLAS ###

Las reglas se codifican.
Contienen tres atributos:
1. Un nombre, de tipo String.
2. Una lista de hechos, que forman las premisas de la regla.
3. Un hecho que es la conclusión de la regla.

### MOTOR ###

Es la pieza central del sistema. El motor de inferencia de razonamiento deductivo.

El método principal es Resolver():
+ Realiza copia local de todas las reglas existentes.
+ Inicializa la base de hechos que originalmente está vacía.
+ Mientras puede aplicarse una regla, el hecho inferido, se agrega a la base de hechos, incrementando su nivel. Se elimina, para no velver a ejecutarla más adelante.
+ Si no quedan más reglas, se muestra el resultado.

### USO ###

La clase Aplicación, tiene el método main() como punto de entrada que ejecutará al método Run(), el cual realizará las tareas:
+ Crear nuevo motor.
+ Agregar las 11 reglas correspondientes a los triángulos y cuadriláteros.
+ Ejecutar la resolución del problema.
