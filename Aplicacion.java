package AI_1.ai_java_ii;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

import AI_1.ai_java_ii.Regla;

public class Aplicacion implements IHM{
    //----------------------------------------------------------------------------------------- main
    public static void main(String[] args) {
        Aplicacion app = new Aplicacion();
        app.Run();
    }
    //----------------------------------------------------------------------------------------- run()
    public void Run() {
        //creacion del motor
        System.out.println("** Creación del motor **");
        MotorInferencia m = new MotorInferencia(this);

        //agregar las reglas
        System.out.println("** Agregadas las reglas **");
        m.agregarRegla("R1 : IF (Orden=3(Cuál es el órden?)) THEN Triángulo");
        m.agregarRegla("R2 : IF (Triángulo AND Ángulo Recto(La figura tiene al menos un ángulo recto?)) THEN Triángulo Rectángulo");
        m.agregarRegla("R3 : IF (Triángulo AND Lados Iguales=2(Cuántos lados iguales tiene la figura?)) THEN Triángulo Isósceles");
        m.agregarRegla("R4 : IF (Triángulo rectángulo AND Triángulo Isósceles) THEN Triángulo Rectángulo Isósceles");
        m.agregarRegla("R5 : IF (Triángulo AND Lados Iguales=3(Cuántos lados iguales tiene la figura?)) THEN Triángulo Equilátero");
        m.agregarRegla("R6 : IF (Orden=4(Cuál es el órden?)) THEN Cuadrilátero");
        m.agregarRegla("R7 : IF (Cuadrilátero AND Lados Paralelos=2(Cuántos lados paralelos entre sí - 0, 2 ó 4?)) THEN Trapecio");
        m.agregarRegla("R8 : IF (Cuadrilátero AND Lados Paralelos=4(Cuántos lados paralelos entre sí - 0, 2 ó 4?)) THEN Paralelogramo");
        m.agregarRegla("R9 : IF (Paralelogramo AND Ángulo Recto(La figura tiene al menos un ángulo recto?)) THEN Rectángulo");
        m.agregarRegla("R10 : IF (Paralelogramo AND Lados Iguales=4(Cuántos lados iguales tiene la figura?)) THEN Rombo");
        m.agregarRegla("R11 : IF (Rectángulo AND Rombo THEN Cuadrado");

        //resolucion
        while (true) {
            System.out.println("\n** Resolución **");
            m.Resolver();
        }
    }
    //-----------------------------------------------------------------------------------------
    @Override
    public int PedirValorEntero(String pregunta) {
        System.out.println(pregunta);
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            return Integer.decode(br.readLine());
        } catch (Exception e) {
            //TODO: handle exception
            return 0;
        }        
    }
    //-----------------------------------------------------------------------------------------
    // solicita un valor booleano, con si (verdadero) o no.
    // Los errores se ignoran (devuelve falso)
    //-----------------------------------------------------------------------------------------
    @Override
    public boolean PedirValorBoolean(String pregunta) {
        try {
            System.out.println(pregunta+ " (si, no)");
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String res = br.readLine();
            return (res.equals("si"));
        } catch (Exception e) {
            return false;
        }        
    }
    //-----------------------------------------------------------------------------------------
    @Override
    public void MostrarHechos(ArrayList<IHecho> hechos) {
        String res = "Solucion(es) encontrada(s) : \n";

        Collections.sort(hechos, (IHecho f1, IHecho f2) -> {
            return Integer.compare(f1.Nivel(), f2.Nivel());
        });

        for (IHecho h : hechos) {
            if (h.Nivel() != 0) {
                res += h.toString() + "\n";
            }
        }
        System.out.println(res);
    }

    //-----------------------------------------------------------------------------------------
    @Override
    public void MostrarReglas(ArrayList<Regla> reglas) {
        String res = "";

        for (Regla r : reglas) {
            res += r.toString() + "\n";
        }
        System.out.println(res);        
    }
    //-----------------------------------------------------------------------------------------

}
